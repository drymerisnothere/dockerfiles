# Ansible

This image will expect to find your reveal files at `/ansible`. You may want to use this function:

``` bash
function ansible(){
    docker run -ti -v `pwd`:/ansible registry.daemons.it/ansible "$@"
}
```
