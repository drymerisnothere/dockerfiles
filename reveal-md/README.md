# Reveal-md

With this image you can use Reveal.js with the
[reveal-md](https://github.com/webpro/reveal-md)
plugin without installing npm, which is always nice. This image will expect to
find your reveal files at `/revealjs/files`. You may use this image with the
next function:

``` bash
reveal () {
	async () {
		sleep 3 && xdg-open http:localhost:8000
	}
	async &|
	docker run -ti --name reveal --rm -v `pwd`:/revealjs/files/ -p 1948:1948 registry.daemons.it/reveal-md
}
```

You may use a css called `presentation.css` and a MD preprocessor called
`preproc.js`.
