FROM alpine:3.7 as build
RUN apk add -U git && wget \
    https://github.com/prometheus/alertmanager/releases/download/v0.16.1/alertmanager-0.16.1.linux-armv7.tar.gz \
    && tar xzf alertmanager-0.16.1.linux-armv7.tar.gz && git clone https://github.com/prometheus/alertmanager/

FROM prom/busybox:latest

COPY --from=build /alertmanager-0.16.1.linux-armv7/amtool /bin/amtool
COPY --from=build /alertmanager-0.16.1.linux-armv7/alertmanager /bin/alertmanager
COPY --from=build /alertmanager/examples/ha/alertmanager.yml /etc/alertmanager/alertmanager.yml

RUN mkdir -p /alertmanager && chown -R nobody:nogroup /etc/alertmanager \
    /alertmanager

USER       nobody
EXPOSE     9093
VOLUME     [ "/alertmanager" ]
WORKDIR    /alertmanager
ENTRYPOINT [ "/bin/alertmanager" ]
CMD        [ "--config.file=/etc/alertmanager/alertmanager.yml", \
             "--storage.path=/alertmanager" ]
