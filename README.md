# Just a bunch of docker files
There should be at least a comment on each dockerfile about how to use it. Ping
me if it doesn't.

All this docker images should be downloadable from my registry at [https://registry.daemons.it](registry.daemons.it).
