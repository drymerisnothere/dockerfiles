# Isso

Pretty simple, just execute [isso](https://posativ.org/isso/).

Example configuration file:

```ini
[general]
dbpath = /isso/comments.db
name = isso
host =
    https://daemons.it
notify = stdout
[server]
listen = http://localhost:8080/
[guard]
enabled = true
ratelimit = 5
direct-reply = 5
reply-to-self = true
require-author = false
require-email = false
[markup]
options = strikethrough, superscript, autolink
[hash]
salt = Eech7co8Ohloopo9Ol6baimi
algorithm = pbkdf2
[moderation]
enabled = false
```

Example oneliner:

``` bash
docker run -ti -v `pwd`:/isso -p8080:8080 test/isso -c isso.cfg
```
