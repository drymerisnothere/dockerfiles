#!/bin/sh

set -ex

FILES="$(git diff HEAD~ --name-only -- '*Dockerfile*')"
WRITE_REGISTRY="${DOCKER_REGISTRY:-r.daemons.it}"

for file in $FILES
do
    dockerfile="$(echo "$file" | sed 's/.*\///')"
    script="$(echo "$file" | sed "s/$dockerfile/push.sh/")"
    docker_name="$(echo "$file" | sed 's/\/.*//')"

    if [ -e "$script" ]
    then
        export WRITE_REGISTRY
        env sh "$script"
    else
        tag="$(echo "$dockerfile" | sed 's/.*_//')"

        if [ "$tag" != "Dockerfile" ]
        then
            push_main_tag="docker push $WRITE_REGISTRY/$docker_name:$tag"
        fi

        cd "$docker_name"
        docker push "$WRITE_REGISTRY/$docker_name:latest"
        eval "$push_main_tag"
        cd ..
    fi
done

set +ex
