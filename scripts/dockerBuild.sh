#!/bin/sh

set -ex

FILES="$(git diff HEAD~ --name-only -- '*Dockerfile*')"
WRITE_REGISTRY="r.daemons.it"

for file in $FILES
do
    dockerfile="$(echo "$file" | sed 's/.*\///')"
    script="$(echo "$file" | sed "s/$dockerfile/build.sh/")"

    if [ -e "$script" ]
    then
        export WRITE_REGISTRY
        env sh "$script"
    else
        docker_name="$(echo "$file" | sed 's/\/.*//')"
        tag="$(echo $file| cut -d '/' -f2)"

        if [ "$(grep -q 'as production' "$file")" ]
        then
            stage_target="--target production"
        fi

        if [ "$tag" != "Dockerfile" ]
        then
            main_tag="-t $WRITE_REGISTRY/$docker_name:$tag"
        fi

        cd "$docker_name"
        eval `echo docker build "$stage_target" "$main_tag" -t \
            "$WRITE_REGISTRY/$docker_name:latest" -f "$dockerfile" . | sed "s/  //g"`
        cd -
    fi
done

set +ex
