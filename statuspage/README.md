# Statuscheck

Pretty simple, just execute
[statuscheck](https://github.com/sourcegraph/checkup/tree/master/statuspage).
Use with
[checkup](https://git.daemons.it/drymer/dockerfiles/src/branch/master/checkup).

Example configuration file:

``` json
checkup.config = {
	"timeframe": 1 * time.Day,
	"refresh_interval": 60,
	"storage": {
	     "url": "http://localhost:2015/check_files"
	},
	"status_text": {
		"healthy": "Situation Normal",
		"degraded": "Degraded Service",
		"down": "Service Disruption"
	}
};
```

Example oneliner:

``` bash
docker run -ti -v `pwd`/config.js:/statuspage/js/config.js -v `pwd`/check_files:/statuspage/check_files -p 2015:2015 registry.daemons.it/statuspage -host localhost
```
