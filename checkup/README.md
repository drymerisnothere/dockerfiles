# Checkup

Pretty simple, just execute [checkup](https://github.com/sourcegraph/checkup).
Use with
[statuspage](https://git.daemons.it/drymer/dockerfiles/src/branch/master/statuspage).

Example configuration file:

``` json
{
     "checkers": [
	     {
		     "type": "tls",
       	     "endpoint_name": "WEB Bad Daemons",
		     "endpoint_url": "daemons.it:443",
		     "attempts": 5
	     },
	     {
		     "type": "tcp",
       	     "endpoint_name": "TCP WEB Bad Daemons",
		     "endpoint_url": "daemons.it:443",
             "tls": true,
		     "attempts": 5
	     }
     ],
     "storage":
     {
         "provider": "fs",
	     "dir": "/checkup/check_files",
         "url": "http://localhost:2015/check_files"
     }
}
```

Example oneliner:

``` bash
docker run -ti -v `pwd`/checkup.json:/checkup/checkup.json -v /etc/ssl/certs:/etc/ssl/certs/ -v `pwd`/check_files:/checkup/check_files registry.daemons.it/checkup
```
