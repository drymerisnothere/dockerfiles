# Weechat

This image will expect to find your configuration files at `/weechat`. You may
want to use this function:

``` bash
docker run -ti --name weechat --rm -v ~/.weechat:/weechat --net=host \\
    registry.daemons.it/weechat
```
