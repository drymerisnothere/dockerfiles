# Hugo

Pretty simple, it just executes [hugo](https://gohugo.io/). Mount your blog in
`/hugo` and let it go:

``` bash
docker run -ti --user 1000:1000 -v `pwd`:/hugo -v /tmp:/tmp -p 1313:1313 \
        registry.daemons.it/hugo:latest "$@"
```
